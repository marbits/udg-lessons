# udg-lessons

GitLab repository for CLI lessons @UdG.

This directory contains:  
- This README file  
- A directory named `00_materials` with several files and directories  
- A code directory with a perl script  

Cheers!
